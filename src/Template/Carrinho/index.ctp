<?php
$loja = $this->loadHelper('Loja');
?>
<div class="usuarios index large-12 medium-11 columns content">
    <h2 class="sub-header">Carrinho</h2>
    <form method="POST" target="_SELF" name="formEnviaPedido" id="formEnviaPedidos" action="http://www.bolaotricolor.com.br/webroot/mobly/Pedidos/add">
    <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Quantidade</th>
                  <th> Valor Unitário</th>
                  <th>Valor Total</th>
                  <th>Excluir</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(isset($_SESSION['carrinho'])&&count($_SESSION['carrinho'])>0){
                  $total = 0;
                  foreach($_SESSION['carrinho'] as $carrinho){
                    $total = $total  + $carrinho['preco_total'];
                  ?>
                    <tr>
                      <td><img src="<?php echo $carrinho['imagem']?>" width="80"> <?php echo $carrinho['titulo']?></td>
                      <td> <input type="number" class="form-control small btnChangeQtd" rel="btnChangeQtd" dataProduto="<?php echo $carrinho['id_produto']?>" value="<?php echo $carrinho['qtd']?>"></td>
                      <td> R$ <?php echo $loja->formata($carrinho['preco'])?></td>
                      <td> R$ <?php echo $loja->formata($carrinho['preco_total'])?></td>
                      <td> <span style="cursor:pointer" class="glyphicon glyphicon-trash" aria-hidden="true" rel="btnDeleteProd" dataProduto="<?php echo $carrinho['id_produto']?>"></span></td>
                   </tr>

                  <?php
                  }
                }
                else {
                ?>
                  <tr>
                      <td colspan="5">Carrinho Vazio</td>
                    </tr>

                <?php
                }
                ?>

              </tbody>
            </table>
            <?php
            if(isset($total)){
             ?>
            <div class="text-right"><h2> Total R$ <?php echo $loja->formata($total) ?></h2><div>
            <div class="text-right"><button type="submit" class="btn btn-primary large">Finalizar Pedido</button><div>
            <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
            <?php
            }
            ?>
          </div>
      </form>
</div>
<script>
    $('input[rel=btnChangeQtd]').change(function(){
    var id_produto = $(this).attr('dataProduto');
    var qtd = $(this).val();
    document.location.href = 'http://www.bolaotricolor.com.br/webroot/mobly/Carrinho/index/'+id_produto + "/"+qtd;
    });

    $('span[rel=btnDeleteProd]').click(function(){
        var id_produto = $(this).attr('dataProduto');
          document.location.href = 'http://www.bolaotricolor.com.br/webroot/mobly/Carrinho/index/'+id_produto;
    });
</script>
