<?php
$loja = $this->loadHelper('Loja');
?>
<div class="usuarios index large-12 medium-11 columns content">
    <h2 class="sub-header">Pedidos</h2>
    <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Código do Pedido</th>
                  <th>Cliente</th>
                  <th>Data do Pedido</th>
                  <th>Total</th>

                </tr>
              </thead>
              <tbody>
                <?php
                if(isset($pedido)&&count($pedido)>0){

                  foreach($pedido as $ped){


                  ?>
                    <tr>
                      <td> <?php echo $ped->id ?></td>
                      <td>  <?php echo "Mobly" ?></td>
                      <td>  <?php echo  $loja->formataData($ped->data_pedido) ?></td>
                      <td> R$ <?php echo $loja->formata($ped->total)?></td>

                   </tr>

                  <?php
                  }
                }
                else {
                ?>
                  <tr>
                      <td colspan="4">Nenhum Pedido Realizado</td>
                    </tr>

                <?php
                }
                ?>

              </tbody>
            </table>

          </div>

</div>
