<?php
$loja = $this->loadHelper('Loja');
 ?>
<div class="usuarios index large-9 medium-8 columns content">
  <form name="formDetalhes" method="POST" id="formDetalhes" action="http://www.bolaotricolor.com.br/webroot/mobly/Carrinho/addShopCart" target="_self">
  <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading"><?php echo $produto[0]->titulo ?>.
            <p class="clear"></p>
          <span class="text-muted">R$ <?php echo  $loja->formata($produto[0]->preco)?></span></h2>
          <p class="lead"><?php echo $produto[0]->descricao ?></p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/300x300/auto" alt="300x300" src="<?php echo $produto[0]->imagem ?>" data-holder-rendered="true">
        </div>
      </div>
      <p class="clear"></p>
      <div class="form-group">

        <div class="col-sm-10">
        <input type="number" class="form-control" id="qtd" name="qtd" placeholder="Quantidade" value="1">
        </div>
        <div class="col-sm-2">
          <input type="hidden" id="id_produto" name="id_produto" value="<?php echo $produto[0]->id ?>">
            <button type="submit" class="btn btn-primary">Adicionar ao carrinho</button>
        </div>

      </div>
    </div>
  </form>
</div>
