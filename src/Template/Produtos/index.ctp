<?php
$loja = $this->loadHelper('Loja');
 ?>
<div class="usuarios index large-9 medium-8 columns content">
  <?php
  foreach($produtos as $produto){
  ?>
  <div class="row">
    <div class="col-lg-4">
      <a target="_self" href="http://www.bolaotricolor.com.br/webroot/mobly/Produtos/detalhes/<?= $produto->id?>">
        <img class="img-circle" src="<?php echo $produto->imagem ?>" alt="" width="140" height="140">
        <p><?php echo  $produto->titulo?></p>
        <h3>R$ <?php echo  $loja->formata($produto->preco)?></h3>
      </a>
    </div>
  </div>
  <?php
  }
  ?>
</div>
