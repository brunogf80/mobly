<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pedidos Controller
 *
 * @property \App\Model\Table\PedidosTable $Pedidos
 */
class PedidosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Pedidos->find('all', [
        'order' => ['id' => 'DESC']
        ]);

        $pedido = $query->toArray();
        $this->set(compact('pedido'));
        $this->set('_serialize', ['pedido']);
    }
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $pedido = $this->Pedidos->newEntity();
      if ($this->request->is('post')) {
          $valores = array(
            'id_cliente'=>1,//fixo 1 para o teste
            'data_pedido'=>date('Y-m-d H:i:s'),
            'total'=>$this->request->data('total')
          );
          $pedido = $this->Pedidos->patchEntity($pedido, $valores);
          if ($this->Pedidos->save($pedido)) {
            //  $this->Flash->success(__('The usuario has been saved.'));
              $this->loadModel('Itens');
              $query = $this->Pedidos->find('all', [
              'order' => ['id' => 'DESC']
              ]);
              $row = $query->first();

              $row = $row->toArray();
              $id_pedido = $row['id'];
              $contador = 0;
              foreach ($_SESSION['carrinho'] as $id_produto=> $value) {
                # code...
                    $item_pedido = $this->Itens->newEntity();
                    $val_itens = array(

                      'id_pedido'=>$id_pedido,
                      'id_produto'=>$id_produto,
                      'preco_unitario'=>$value['preco'],
                      'quantidade'=>$value['qtd'],
                      'preco_total'=>$value['preco_total']
                    );
                    $item_pedido = $this->Itens->patchEntity($item_pedido, $val_itens);



                    if ($this->Itens->save($item_pedido)) {
                    $contador++;
                    }
                    else{
                      debug($this->Itens->errors());
                    }
              }
              //se salvou todos, limpa o carrinho
              if($contador==count($_SESSION['carrinho']))
              {
                unset($_SESSION['carrinho']);
              }
              return $this->redirect(['action' => 'index']);
          }
      }
    }
}
