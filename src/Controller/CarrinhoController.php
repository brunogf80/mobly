<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Description of ProdutosController
 *
 * @author Bruno
 */
class CarrinhoController extends AppController
{
    //put your code here
     /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

     public function index($id_produto = null, $qtd = null)
     {
        if($id_produto!=null &&$qtd==null){
          unset($_SESSION['carrinho'][$id_produto]);
        }

        if($qtd!=null && $id_produto!=null){
          $_SESSION['carrinho'][$id_produto]['qtd'] = $qtd;
          $_SESSION['carrinho'][$id_produto]['preco_total'] =   $_SESSION['carrinho'][$id_produto]['preco'] *   $_SESSION['carrinho'][$id_produto]['qtd'];
        }
    }

     //put your code here
      /**
      * addShopCart method
      *
      * @return \Cake\Network\Response|null
      */
    public function addShopCart()
    {

        if(null === $this->request->data('id_produto')){
            return $this->redirect(
             ['controller' => 'Categorias', 'action' => 'index']
           );
        }

        $id_produto = $this->request->data('id_produto');

        $produto = $this->Produtos->find('all',[
            'join' =>
            [
                'table' => 'categorias',
                'alias' => 'Categorias',
                'type' => 'INNER',
                 'conditions' =>
                'Produtos.id_categoria = Categorias.id'

            ],
            //'fields'=>"*",
            'conditions' =>  "Produtos.id = $id_produto"
        ]

        );


      $produto  = $produto->toArray();

      $_SESSION['carrinho'][$id_produto]['imagem'] = $produto[0]->imagem;
      $_SESSION['carrinho'][$id_produto]['titulo'] = $produto[0]->titulo;
      $_SESSION['carrinho'][$id_produto]['descricao'] = $produto[0]->descricao;
      $_SESSION['carrinho'][$id_produto]['qtd'] = !isset(  $_SESSION['carrinho'][$id_produto]['qtd'])? $this->request->data('qtd'): $_SESSION['carrinho'][$id_produto]['qtd']+$this->request->data('qtd');
      $_SESSION['carrinho'][$id_produto]['preco'] = $produto[0]->preco;
      $_SESSION['carrinho'][$id_produto]['preco_total'] = $produto[0]->preco *   $_SESSION['carrinho'][$id_produto]['qtd'];
      $_SESSION['carrinho'][$id_produto]['categoria'] = $produto[0]->id_categoria;
      $_SESSION['carrinho'][$id_produto]['id_produto'] = $id_produto;

      $this->setAction('index');


    }



}
