<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Description of ProdutosController
 *
 * @author Bruno
 */
class ProdutosController extends AppController
{
    //put your code here
     /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id_categoria=null)
    {

        $produtos = $this->Produtos->find('all',[
            'join' =>
            [
                'table' => 'categorias',
                'alias' => 'Categorias',
                'type' => 'INNER',
                 'conditions' =>
                'Produtos.id_categoria = Categorias.id'

            ],


            //'fields'=>"*",
            'conditions' =>  "Produtos.id_categoria = $id_categoria"
        ]

        );


        $this->viewBuilder()->layout('mobly');
        $this->set(compact('produtos'));

    }

    /**
    * Detalhes method
    *
    * @return \Cake\Network\Response|null
    */
   public function detalhes($id_produto=null)
   {

       $produto = $this->Produtos->find('all',[
           'join' =>
           [
               'table' => 'categorias',
               'alias' => 'Categorias',
               'type' => 'INNER',
                'conditions' =>
               'Produtos.id_categoria = Categorias.id'

           ],
           //'fields'=>"*",
           'conditions' =>  "Produtos.id = $id_produto"
       ]

       );

       $produto = $produto->toArray();

       $this->viewBuilder()->layout('mobly');
       $this->set(compact('produto'));

   }
}
