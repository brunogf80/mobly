<?php


namespace App\View\Helper;

use Cake\View\Helper;

class LojaHelper extends Helper
{
    public function formata($preco)
    {
        // Logic to create specially formatted link goes here...
        return number_format($preco, 2, ',', ' ');
    }

    public function formataData($data){
      $date = date_create($data);
      return date_format($date,'d/m/Y H:i:s');
    }
}
