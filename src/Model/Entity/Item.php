<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemPedido Entity
 *
 * @property int $id
 * @property int $id_pedido
 * @property int $id_produto
 * @property double $preco_unitario
 * @property double $total
  * @property int $quantidade
 * @property double $preco_total

 */
class ItemPedido extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];

  
}
