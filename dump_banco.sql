-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 186.202.152.32
-- Generation Time: 21-Jan-2018 às 04:08
-- Versão do servidor: 5.6.38-83.0-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teste_mobly`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`) VALUES
(2, 'Camisetas Masculinas'),
(3, 'CDS, DVS, Blue Ray e Box'),
(4, 'Canecas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_pedidos`
--

CREATE TABLE `item_pedidos` (
  `id` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `preco_unitario` double(6,2) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `preco_total` double(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL DEFAULT '1' COMMENT 'cliente fixo para o teste',
  `data_pedido` datetime NOT NULL,
  `total` double(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `descricao` text NOT NULL,
  `preco` double(6,2) NOT NULL,
  `imagem` varchar(150) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `descricao`, `preco`, `imagem`, `id_categoria`) VALUES
(3, 'Camiseta Masculina Paralamas do Sucesso - 30 Anos Modelo 1', 'Modelo:  30 Anos Modelo 1\r\nCor: Preta com estampa em impressão digital\r\nComposição: 100% Algodão\r\nMedidas: P / M / G / GG', 69.90, 'https://images.tcdn.com.br/img/img_prod/317999/180__116_1_20140425095808.jpg', 2),
(4, 'Caneca Os Paralamas do Sucesso - Face', 'Modelo: Face\r\n\r\nCapacidade: 300ml\r\n\r\nCor: Branco estampado\r\n\r\nComposição: Porcelana Esmaltada', 34.80, 'https://images.tcdn.com.br/img/img_prod/317999/180_54_1_20131210154513.jpg', 4),
(5, 'DVD Os Paralamas do Sucesso Brasil Afora Ao Vivo Multishow', 'Músicas do mais recente álbum da banda (Brasil Afora) estão no repertório, assim como os hits imortais, como: “Meu Erro”, “Alagados”, “Óculos”, “Lanterna dos Afogados”, “Vital e sua Moto”, entre outros. Um dos pontos altos do show é um set acústico onde a banda revisita músicas de seu repertório com nova roupagem. Destacam-se também as participações especiais de Pitty em “Tendo a Lua” e do “mestre” Zé Ramalho em “Mormaço”. ', 34.90, 'https://images.tcdn.com.br/img/img_prod/317999/180_dvd_paralamas_do_sucesso_brasil_afora_ao_vivo_multishow_263_1_20160928102411.jpg', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_pedidos`
--
ALTER TABLE `item_pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pedido` (`id_pedido`),
  ADD KEY `item_pedido_ibfk_2` (`id_produto`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_categoria_2` (`id_categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `item_pedidos`
--
ALTER TABLE `item_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `item_pedidos`
--
ALTER TABLE `item_pedidos`
  ADD CONSTRAINT `item_pedidos_ibfk_1` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `item_pedidos_ibfk_2` FOREIGN KEY (`id_produto`) REFERENCES `produtos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `fk_id_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
